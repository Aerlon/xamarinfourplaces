﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class AddCommentViewModel : ViewModelBase
    {
        private string _commentEntry;
        private PlaceItem _placeItem;
        private int _idPlace;
        private bool _warningVisible;
        private string _warningText;
        public ICommand AddCommentCommand { get; }
        
        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }
        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }

        public string CommentEntry
        {
            get => _commentEntry;
            set => SetProperty(ref _commentEntry, value);
        }
        public AddCommentViewModel(PlaceItem placeItem)
        {
            _idPlace = placeItem.Id;
            _placeItem = placeItem;
            AddCommentCommand = new Command(AddComment);
        }
        public async void AddComment()
        {
            ApiClient apiClient = new ApiClient();
            CreateCommentRequest createCommentRequest = new CreateCommentRequest();
            createCommentRequest.Text = CommentEntry;
            HttpResponseMessage response2 = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/places/"+_idPlace+"/comments", createCommentRequest, App.token);
            if (response2.IsSuccessStatusCode)
            {
                Console.WriteLine("Add Comment Success");
                WarningVisible = true;
                WarningText = "Envoi réussi";
                await App.NavigationService.NavigateAsync("SeeCommentsPage", _placeItem);
            }
            else
            {
                Console.WriteLine("Add Comment Failed");
                WarningVisible = true;
                WarningText = "Echec de l'envoi du commentaire";
            }
        }
    }
}
