﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class LoginViewModel : ViewModelBase
    {
        private string _loginEntry;
        private string _mdpEntry;
        private bool _warningVisible;
        private string _warningText;

        public ICommand PostLoginMdpCommand { get; }
        public ICommand GoToRegisterPageCommand { get; }

        public string LoginEntry
        {
            get => _loginEntry;
            set => SetProperty(ref _loginEntry, value);
        }
        public string MdpEntry
        {
            get => _mdpEntry;
            set => SetProperty(ref _mdpEntry, value);
        }
        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }
        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }
        public LoginViewModel()
        {
            PostLoginMdpCommand = new Command(async () => await PostLoginMdp());
            GoToRegisterPageCommand = new Command(GoToRegisterPage);
        }
        public async Task PostLoginMdp()
        {
            ApiClient apiClient = new ApiClient();
            LoginRequest loginrequest = new LoginRequest();
            loginrequest.Email = LoginEntry;
            loginrequest.Password = MdpEntry;
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/auth/login", loginrequest);
            Response<LoginResult> result = await apiClient.ReadFromResponse<Response<LoginResult>>(response);
            if (result.IsSuccess)
            {
                WarningVisible = true;
                WarningText = "Connexion en cours...";
                App.token = result.Data.AccessToken;
                await App.NavigationService.NavigateAsync("HomePage");
            }
            else if(result.ErrorMessage == "Invalid credentials")
            {
                WarningVisible = true;
                WarningText = "Email ou mot de passe invalide";
            }
            else
            {
                WarningVisible = true;
                WarningText = "Connexion impossible";
            }
        }
        public async void GoToRegisterPage()
        {
            await App.NavigationService.NavigateAsync("RegisterPage");
        }
    }
}
