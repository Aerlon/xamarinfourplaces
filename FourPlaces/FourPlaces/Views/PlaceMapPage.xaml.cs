﻿using FourPlaces.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlaceMapPage : ContentPage
    {
        /*public PlaceMapPage()
        {
            InitializeComponent();
            BindingContext = new PlaceMapViewModel();
        }*/
        public PlaceMapPage(PlaceItem place)
        {
            InitializeComponent();
            Position position = new Position(place.Latitude, place.Longitude);
            MapSpan mapSpan = new MapSpan(position, 0.01, 0.01);
            Carte.MoveToRegion(mapSpan);
            var pin = new Pin()
            {
                Position = position,
                Label = place.Title
            };
            Carte.Pins.Add(pin);
            Desc.Text = place.Description;
            BindingContext = new PlaceMapViewModel(place);
        }
    }
}